# Spring App Config Server
### Information
A very basic Spring app config server with a local or remote Git repo sample that is commented in application.yml

##### As part of series:
* Git repo for Config Server (used by this project)
* Eureka Spring server (used by "Spring App sample" project)
* Spring App sample (uses Config Server & Eureka)
* Spring App Cloud Gateway server (uses Eureka & "Spring App sample")

#### How it is achieved
in pom.xml:

```xml
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-config-server</artifactId>
        </dependency>
```

Java Main class as simples as:

```java
@SpringBootApplication
@EnableConfigServer
public class ConfigServer {
    public static void main(String[] args) {
        SpringApplication.run(ConfigServer.class, args);
    }
}
```

#### Sample features
- May use a "local" (simulated) Git repository

```yaml
spring:
  cloud:
    config:
      server:
        git:
          uri: file:///${user.home}/myFolder/spring-app-config-repo
          search-paths:
          - content
```
- May use a Git repository, see "Git repo for Config Server"

```yaml
spring:
  cloud:
    config:
      server:
        git:
          uri: https://gitlab.com/spring-apps-infra/spring-app-repo-samples.git
          search-paths:
          - content
#          username: my_username
#          password: my_password
```